object zad1 {

  val dniTygodnia = List("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday", "SrodekTygodnia", "Sroda popielcowa")

  def runZad(): Unit = {
    println("zad1")
    println(dniTygodnia.map(day => checkDayType(day)))
  }

  def checkDayType(day: String): String = day match {
    case "Monday" | "Tuesday" | "Wednesday" | "Thursday" | "Friday" => "Praca"
    case "Saturday" | "Sunday" => "Weekend"
    case _ => "Nie ma takiego dnia"
  }

}
