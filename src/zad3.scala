object zad3 {
  def runZad(): Unit = {
    println("zad3")
    println(sayHello(Osoba("A", "A")))
    println(sayHello(Osoba("Adrian", "Pusty")))
    println(sayHello(Osoba("Michal","Michalowski")))
    println(sayHello(Osoba("Michal","Maklowicz")))
  }

  case class Osoba(imie: String, nazwisko: String)

  def sayHello(osoba: Osoba) = osoba match {
    case Osoba("Adrian", _) => "No czeeesc"
    case Osoba(i, "Nowakowski") => s"Typical $i Kowalski"
    case Osoba("Michal", "Michalowski") => "Hello Mr MM"
    case Osoba(i,n) => s"Hi $i , $n , I dont know you... stranger...."
  }


}
