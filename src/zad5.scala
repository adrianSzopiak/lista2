
object zad5 {
  def runZad(): Unit = {
    println("zad5")
    println(new Osoba(_podatek = 30).podatek)
    println((new Osoba(_podatek =  80) with Student).podatek)
    println(new Osoba(_podatek =  80) with Nauczyciel {pensja = 1000}.podatek)
    println(new Osoba(_podatek =  80) with Student with Nauczyciel {pensja = 1000}.podatek)
    println(new Osoba(_podatek =  80) with Pracownik {pensja = 1000}.podatek)
    println(new Osoba(_podatek =  80) with Nauczyciel with Pracownik {pensja = 1000}.podatek)
    println(new Osoba(_podatek =  80) with Pracownik with Nauczyciel {pensja = 1000}.podatek)
  }

  class Osoba(val imie: String = "a", val nazwisko: String = "b", val _podatek: Double) { def podatek: Double = _podatek}

  trait Student extends Osoba {
    override def podatek: Double = 0
  }

  trait Nauczyciel extends Pracownik {
    override def podatek: Double = pensja * 0.9
  }

  trait Pracownik extends Osoba {
    var pensja: Double = 0.0

    override def podatek: Double = pensja * 0.8
  }

}
