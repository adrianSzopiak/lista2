object zad2 {
  def runZad(): Unit = {
    println("zad2")
    val konto1 = new BankAccount()
    val konto2 = new BankAccount(123)
    konto1.wyplata(50)
    konto2.wyplata(50)
    println(konto1.stanKonta)
    println(konto2.stanKonta)
  }

  class BankAccount(private val initialBalance: Double = 0.0) {

    private var balance = initialBalance

    def stanKonta: Double = balance

    def wyplata(amount: Double) = {
      if (amount > 0 && amount <= balance) balance -= amount
    }

    def wplata(amount: Double) = {
      if (amount > 0) balance += amount
    }
  }

}
