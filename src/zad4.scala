object zad4 {
  def runZad(): Unit = {
    println("zad4")
    println(evaluateValue(2, value => value * 2))
  }

  def evaluateValue(value: Int, function: (Int) => Int): Int = function(function(function(value)))
}
